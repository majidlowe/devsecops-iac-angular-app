# Prerequisites
    Download node_module folder in the directory
    Run `npm install -g @anglular/cli` to install angular cli.
    Download javaopenjdk and setup JAVA_HOME environment variables.

# DevsecopsIacAngularApp
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Set up Protractor and Cucumber for e2e test coverage
1. install dev dependencies
``` 
npm install --save-dev @types/chai @types/cucumber
```
2. install dependencies
``` 
npm install --save cucumber chai protractor-cucumber-framework
```
3. open e2e/tsconfig.json file and add replace:

    "types": ["jasmine", "jasminewd2", "node"]

    with

    "types": ["chai", "cucumber", "node"]

4. Update protractor.conf.js to use cucumber:
    
    1. Update files to be used:
        specs: ['./src/features/**/*.feature']
    2. Tell protractor to use cucumber framwork:
        framework: 'custom'
        frameworkPath: requirer.resolve('protractor-cucumber-framework')
    3. Configure cucumber options: 
        cucmberOpts: {
            require: ['./src/steps/**/*.steps.ts]
        }
    4. remove any Jasmine specific code


5. add features, pages and steps folders
6. move app.po.ts to the pages folder

## Install and set up sonar-scanner
1. run:
```
npm install --save sonar-scanner
```

2. add sonar-project.properties to .gitignore file

3. create sonar-project.properties file

4. add configuration to sonar-project.properties:
    ```
    sonar.host.url=<url>
    sonar.login=<login>
    sonar.password=<password>
    sonar.projectKey=<project key set in sonarqube>
    sonar.projectName=DevSecOps-<project name>
    sonar.projectVersion=1.0
    sonar.sourceEncoding=UTF-8
    sonar.sources=src
    sonar.exclusions=**/node_modules/**
    sonar.tests=src
    sonar.test.inclusions=**/*.steps.ts
    sonar.typescript.lcov.reportPaths=coverage/<path to lcov.info file>
    ```

5. Add following to package.json scripts array:
    "sonar": "sonar-scanner"

6. run:
```
npm run sonar
```

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
